const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');


//Register
router.post('/register', (req, res, next)=>{
    let newUser = new User({
        fname: req.body.fname,
        lname:req.body.lname,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });
    User.addUser(newUser, (err, user)=>{
        if(err){
            res.json({success: false, msg: 'Failed to register user'});
        }
        else{
            res.json({success: true, msg: 'User Registered'});
        }
    })
});

//Auth
router.post('/auth', (req, res, next)=>{
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user)=>{
        if(err) throw err;
        if(!user){
            return res.json({success:false, msg:'User not found'})
        }
        User.comparePassword(password, user.password, (err, isMatch)=>{
            if(err) throw err;
            if(isMatch){
                const token = jwt.sign({data: user}, config.secret, {
                    expiresIn: 604800
                });
                res.json({
                    success:true,
                    token: `Bearer ${token}`,
                    user:{
                        id: user._id,
                        fname: user.fname,
                        lname: user.lname,
                        username: user.username,
                        email:user.email,
                        connections:user.connections
                    }
                });
            }
            else{
                return res.json({
                    success: false,
                    msg: 'Incorrect password'
                });
            }
        })
    })
});

//Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    res.json({
        use: req.user
    });
});

router.get('/addconnection/:username', passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const username = req.params.username;
    User.getUserByUsername(username, (error, isThere)=>{
        if(error){
            res.json({
                success:false,
                msg:'Error occured 1'
            })
        }
        if(isThere){
            //add connection
            User.addConnection(req.user._id, username, (err, done)=>{
                if(err){
                    res.json({
                        success:false,
                        msg:'Error Occured'
                    })
                }
                if(done){
                    User.addConnection(isThere._id, req.user.username, (err2, done2)=>{
                        if(err2){
                            res.json({
                                success:false,
                                msg:'Error2 Occured'
                            })
                        }
                        if(done2){
                            res.json({
                                success:true,
                                msg:'Connection added'
                            })
                        }else{
                            res.json({
                                success:false,
                                msg:'Failed to add connection2'
                            })
                        }
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to add connection1'
                    })
                }
            })
        }
        else{
            res.json({
                success:false,
                msg:'User does not found'
            })
        }
    })
})


module.exports = router;