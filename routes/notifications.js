const express = require('express');
const router = express.Router();
const passport = require('passport');

const Notification = require('../models/notification');


router.get('/all', passport.authenticate('jwt', { session: false }), (req, res, next) =>{
    const username = req.user.username;
    Notification.getAll(username, (err, nots)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error occured'
            })
        }
        if(nots){
            res.json({
                success:true,
                msg:'Notifications fetched',
                notifications:nots
            })
        }else{
            res.json({
                success:false,
                msg:'Failed to fetch notifications'
            })
        }
    })
})
router.get('/new', passport.authenticate('jwt', { session: false }), (req, res, next) =>{
    const username = req.user.username;
    Notification.checkNew(username, (err2, isAvailable)=>{
        if(err2){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }
        if(isAvailable){
            //Fetch unviewed
            Notification.getUnviewed(username, (err, nots)=>{
                if(err){
                    res.json({
                        success:false,
                        msg:'Error occured'
                    })
                }
                if(nots){
                    Notification.allViewed(username, (err3, done)=>{
                        if(err3){
                            res.json({
                                success:true,
                                msg:'Notifications fetched',
                                hasNew:true,
                                notifications:nots
                            })
                        }
                        if(done){
                            res.json({
                                success:true,
                                msg:'Notifications fetched',
                                hasNew:true,
                                notifications:nots
                            })
                        }else{
                            res.json({
                                success:true,
                                msg:'Notifications fetched',
                                hasNew:true,
                                notifications:nots
                            })
                        }
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to fetch notifications'
                    })
                }
            })
        }else{
            res.json({
                success:true,
                msg:'No new notifications',
                hasNew:false
            })
        }
    })
})



module.exports = router;