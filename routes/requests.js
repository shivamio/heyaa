const express = require('express');
const router = express.Router();
const passport = require('passport');
const Request = require('../models/request');
const User = require('../models/user');

router.get('/received',  passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const username = req.user.username
    Request.received(username, (err, reqs)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }
        if(reqs){
            res.json({
                success:true,
                msg:'Requests found',
                requests:reqs
            })
        }else{
            res.json({
                success:false,
                msg:'Failed to get requests'
            })
        }
    })
})

router.get('/sent',  passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const username = req.user.username
    Request.sent(username, (err, reqs)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }
        if(reqs){
            res.json({
                success:true,
                msg:'Requests found',
                requests:reqs
            })
        }else{
            res.json({
                success:false,
                msg:'Failed to get requests'
            })
        }
    })
})

router.get('/send/:toUser', passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const toUser = req.params.toUser;
    const fromUser = req.user.username;

    Request.new(toUser,fromUser, (err,done)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error occured'
            })
        }
        if(done){
            res.json({
                success:true,
                msg:'Request sent'
            })
        }
        else{
            res.json({
                success:false,
                msg:'Failed to send request'
            })
        }
    })
})

router.get('/approve/:fromUser', passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const fromUser = req.params.fromUser;
    const toUser = req.user.username;
    const toUserId = req.user._id;
    Request.from(fromUser,toUser, (err,isThere)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }
        if(isThere){
            //Add Connection
            //Update Request to Approved:True
            User.getUserByUsername(fromUser, (err2,found)=>{
                if(err2){
                    res.json({
                        success:false,
                        msg:'Error occured'
                    })
                }
                if(found){
                    User.addConnection(toUserId,found.username, (err3, done1)=>{
                        if(err3){
                            res.json({
                                success:false,
                                msg:'Error Occured'
                            })
                        }
                        if(done1){
                            User.addConnection(found._id, toUser, (err4, done2)=>{
                                if(err4){
                                    res.json({
                                        success:false,
                                        msg:'Error Occured'
                                    })
                                }
                                if(done2){
                                    res.json({
                                        success:true,
                                        msg:'Connection added'
                                    })
                                }else{
                                    res.json({
                                        success:false,
                                        msg:'failed to add connection'
                                    })
                                }
                            })
                        }
                        else{
                            res.json({
                                success:false,
                                msg:'Failed to add connection'
                            })
                        }
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Request sending user not found'
                    })
                }
            })
        }
        else{
            res.json({
                success:false,
                msg:'No request found'
            })
        }
    })
})

router.get('/deny/:fromuser', passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const toUser = req.user.username;
    const fromUser = req.params.fromuser;
    if(fromUser != undefined){
        Request.from(fromUser,toUser, (err, isThere)=>{
            if(err){
                res.json({
                    success:false,
                    msg:'Error occured'
                })
            }
            if(isThere){
                Request.deny(toUser,fromUser, (err2, done)=>{
                    if(err2){
                        res.json({
                            success:false,
                            msg:'Error Occured'
                        })
                    }
                    if(done){
                        res.json({
                            success:true,
                            msg:'Request denied'
                        })
                    }else{
                        res.json({
                            success:false,
                            msg:'failed to deny request'
                        })
                    }
                })
            }else{
                res.json({
                    success:false,
                    msg:'Request not found'
                })
            }
        })
    }else{
        res.json({
            success:false,
            msg:'sender not supplied'
        })
    }
})

module.exports = router;