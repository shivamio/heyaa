const express = require('express');
const router = express.Router();
const passport = require('passport');
const date = require('date-and-time');

const Chat = require('../models/chat');

router.get('/all',  passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const user = req.user.username;
    Chat.getAll(user, (err,chats)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }
        if(chats){
            res.json({
                success:true,
                msg:'Chats Available',
                chats:chats
            })
        }else{
            res.json({
                success:false,
                msg:'Failed to fetch chats'
            })
        }
    })
})

router.get('/from/:otheruser', passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const userA = req.user.username;
    const userB = req.params.otheruser;
    Chat.between(userA,userB, (err, chats)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }
        if(chats){
            res.json({
                success:true,
                msg:'Chats Available',
                chats:chats
            })
        }else{
            res.json({
                success:false,
                msg:'Failed to fetch chats'
            })
        }
    })
})

router.post('/message/:otheruser',passport.authenticate('jwt', {session:false}), (req, res, next)=>{
    const userA = req.user.username;
    const userB = req.params.otheruser;
    var tempNow = new Date();
    var now = date.format(tempNow,'hh:mm A MMM DD YYYY');
    var newMsg = {
        to:userB,
        from:userA,
        timestamp:now,
        message:req.body.message
    }
    Chat.between(userA,userB, (err, exists)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error occured'
            })
        }
        if(exists){
            Chat.newMsg(userA,userB,newMsg, (err2, done)=>{
                if(err2){
                    res.json({
                        success:false,
                        msg:'Error occured'
                    })
                }
                if(done){
                    res.json({
                        success:true,
                        msg:'message sent'
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to send message'
                    })
                }
            })
        }
        else{
            //Create Chat then send message
            Chat.createChat(userA,userB, (err3, created)=>{
                if(err3){
                    res.json({
                        success:false,
                        msg:'Error Occured'
                    })
                }
                if(created){
                    //Now send message
                    Chat.newMsg(userA,userB,newMsg, (err4,sent)=>{
                        if(err4){
                            res.json({
                                success:false,
                                msg:'Error Occured'
                            })
                        }
                        if(sent){
                            res.json({
                                success:true,
                                msg:'message sent'
                            })
                        }else{
                            res.json({
                                success:false,
                                msg:'Failed to send message'
                            })
                        }
                    })
                }
                else{
                    res.json({
                        success:false,
                        msg:'Failed to create chat'
                    })
                }
            })
        }
    })
})


module.exports = router;