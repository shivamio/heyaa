const express = require('express');
const router = express.Router();
const passport = require('passport');

const Profile = require('../models/profile');

const ImgUpload = require('../services/imgupload');

const dpUpload = ImgUpload.single('myImage');

router.get('/myprofile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    Profile.getByUsername(username, (err, profile) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error Occured'
            })
        }
        if (profile) {
            res.json({
                success: true,
                msg: 'Profile found',
                profile: profile
            })
        } else {
            res.json({
                success: false,
                msg: 'No profile associated'
            })
        }
    })
})

router.post('/create', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const name = req.user.fname + " " + req.user.lname;
    var newProfile = new Profile({
        user: username,
        name: name,
        dob: req.body.dob,
        gender: req.body.gender,
        livingIn: req.body.livingIn,
        studiedAt: req.body.studiedAt,
        workingAt: req.body.workingAt
    })
    Profile.create(newProfile, (err, done) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error Occured'
            })
        }
        if (done) {
            res.json({
                success: true,
                msg: 'Profile created'
            })
        } else {
            res.json({
                success: false,
                msg: 'Failed to create profile'
            })
        }
    })
})

router.post('/newdp', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    dpUpload(req, res, (err) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error Occured'
            })
        } else {
            var imgPath = './uploads/img/profile/' + req.file.filename;
            Profile.addDpToProfile(username, imgPath, (err2, done) => {
                if (err2) {
                    res.json({
                        success: false,
                        msg: 'Error Occured'
                    })
                }
                if (done) {
                    res.json({
                        success: true,
                        msg: 'Image uploaded'
                    })
                }
                else {
                    res.json({
                        success: false,
                        msg: 'Failed to upload image'
                    })
                }
            })
        }
    })
})

router.get('/user/:targetuser', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const targetUser = req.params.targetuser;
    const connections = req.user.connections;
    var isConnected = false;
    for (let con of connections) {
        if (con == targetUser) {
            isConnected = true;
        }
    }
    Profile.getByUsername(targetUser, (err, isThere) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error occured'
            })
        }
        if (isThere) {
            if (isThere.user == req.user.username) {
                res.json({
                    success: true,
                    isSelf: true,
                    msg: 'Profile fetched',
                    profile: isThere
                })
            } else {
                if (isConnected) {
                    res.json({
                        success: true,
                        isConnection: true,
                        isSelf:false,
                        msg: 'Profile fetched',
                        profile: isThere
                    })
                } else {
                    res.json({
                        success: true,
                        isConnection: false,
                        isSelf:false,
                        msg: 'User not a connection',
                        profile: {
                            name: isThere.name,
                            gender: isThere.gender,
                            profilePic: isThere.profilePic
                        }
                    })
                }
            }
        } else {
            res.json({
                success: false,
                msg: 'User not found'
            })
        }
    })
})

router.post('/update', passport.authenticate('jwt', { session: false }), (req, res, next) =>{
    const username = req.user.username;
    const update = {
        name:req.body.name,
        gender:req.body.gender,
        dob:req.body.dob,
        livingIn:req.body.livingIn,
        studiedAt:req.body.studiedAt,
        workingAt:req.body.workingAt
    }
    Profile.getByUsername(username, (err,isThere)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error occured'
            })
        }
        if(isThere){
            Profile.updateByusername(username, update, (err2,done)=>{
                if(err2){
                    res.json({
                        success:false,
                        msg:'Error occured'
                    })
                }
                if(done){
                    res.json({
                        success:true,
                        msg:'profile updated'
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'failed to update profile'
                    })
                }
            })
        }else{
            res.json({
                success:false,
                msg:'Profile not found'
            })
        }
    })
})

module.exports = router;