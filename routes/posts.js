const express = require('express');
const router = express.Router();
const passport = require('passport');
const Post = require('../models/post');
const date = require('date-and-time');
const Notification = require('../models/notification');
const imgUpload = require('../services/postimg');
const PostImg = imgUpload.single('postImg');
const vidUpload = require('../services/postvid');
const PostVid = vidUpload.single('postVid');

router.get('/all', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const connections = req.user.connections;
    Post.getAll(connections, (err, posts) => {
        if (err) {
            console.log(err)
            res.json({
                success: false,
                msg: 'Error Occured'
            })
        }
        if (posts) {
            res.json({
                success: true,
                msg: 'Posts found',
                posts: posts
            })
        } else {
            res.json({
                success: false,
                msg: 'Failed to fetch posts'
            })
        }
    })
})

router.post('/new', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const now = new Date();
    const timestamp = date.format(now, 'hh:mm A MMM DD YYYY');
    var newPost = new Post({
        user: username,
        dateTime: timestamp,
        content: req.body.content
    })
    Post.addNew(newPost, (err, posted) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error Occured',
            })
        }
        if (posted) {
            res.json({
                success: true,
                msg: 'Post Successful'
            })
        }
        else {
            res.json({
                success: false,
                msg: 'Failed to post'
            })
        }
    })
})

router.post('/newimage', passport.authenticate('jwt', { session: false }), (req, res, next) =>{
    const username = req.user.username;
    const now = new Date();
    const timestamp = date.format(now, 'hh:mm A MMM DD YYYY');
    PostImg(req,res, (err)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }else{
            var imgPath = './uploads/img/posts/' + req.file.filename;
            var newPost = new Post({
                user: username,
                dateTime: timestamp,
                img: imgPath,
                postType:'image'
            })
            Post.addNew(newPost, (err, posted) => {
                if (err) {
                    res.json({
                        success: false,
                        msg: 'Error Occured',
                    })
                }
                if (posted) {
                    res.json({
                        success: true,
                        msg: 'Post Successful'
                    })
                }
                else {
                    res.json({
                        success: false,
                        msg: 'Failed to post'
                    })
                }
            })
        }
    })
})

router.post('/newvideo', passport.authenticate('jwt', { session: false }), (req, res, next) =>{
    const username = req.user.username;
    const now = new Date();
    const timestamp = date.format(now, 'hh:mm A MMM DD YYYY');
    PostVid(req,res, (err)=>{
        if(err){
            res.json({
                success:false,
                msg:'Error Occured'
            })
        }else{
            var vidPath = './uploads/vid/posts/' + req.file.filename;
            var newPost = new Post({
                user: username,
                dateTime: timestamp,
                vid: vidPath,
                postType:'video'
            })
            Post.addNew(newPost, (err, posted) => {
                if (err) {
                    res.json({
                        success: false,
                        msg: 'Error Occured',
                    })
                }
                if (posted) {
                    res.json({
                        success: true,
                        msg: 'Post Successful'
                    })
                }
                else {
                    res.json({
                        success: false,
                        msg: 'Failed to post'
                    })
                }
            })
        }
    })
})

router.get('/from/:username', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const userToFetch = req.params.username;
    const connections = req.user.connections;
    var haveConnect = false;
    for (let cons of connections) {
        if (cons == userToFetch) {
            haveConnect = true
        }
    }
    if (haveConnect) {
        Post.getByUsername(userToFetch, (err, posts) => {
            if (err) {
                res.json({
                    success: false,
                    msg: 'Error Occured'
                })
            }
            if (posts) {
                res.json({
                    success: true,
                    msg: 'Posts fetched',
                    posts: posts
                })
            }
            else {
                res.json({
                    success: false,
                    msg: 'Failed to fetch posts'
                })
            }
        })
    }
    else {
        res.json({
            success: false,
            msg: 'This user is not your connection'
        })
    }
})

router.get('/like/:postid', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const postId = req.params.postid;
    var isAlreadyLiked = false;
    var hasDislikedBefore = false;

    Post.getPostById(postId, (err2, thisPost) => {
        if (err2) {
            res.json({
                success: false,
                msg: 'Error occured'
            })
        }
        if (thisPost) {
            var getdislikes = thisPost.dislikedBy;
            for (let disl of getdislikes) {
                if (disl == username) {
                    hasDislikedBefore = true;
                }
            }
            if (hasDislikedBefore) {
                Post.dislikeToLike(postId, username, (error, changed) => {
                    if (error) {
                        res.json({
                            success: false,
                            msg: 'Error Occured'
                        })
                    }
                    if (changed) {
                        const notifyMsg = username + ' liked your post';
                        const notifyMsgTyp = 'like';
                        Notification.addNew(thisPost.user, notifyMsg, notifyMsgTyp, (er, notified) => {
                            if (er) {
                                res.json({
                                    success: false,
                                    msg: 'Error occured'
                                })
                            }
                            if (notified) {
                                res.json({
                                    success: true,
                                    msg: 'Post Liked'
                                })
                            }
                        })
                    }
                })
            }
            else {
                var likers = thisPost.likedBy;
                for (let liker of likers) {
                    if (liker == username) {
                        isAlreadyLiked = true
                    }
                }
                if (!isAlreadyLiked) {
                    Post.likePost(postId, username, (err, done) => {
                        if (err) {
                            res.json({
                                success: true,
                                msg: 'Error Occured'
                            })
                        }
                        if (done) {
                            const notifyMsg = username + ' liked your post';
                            const notifyMsgTyp = 'like';
                            Notification.addNew(thisPost.user, notifyMsg, notifyMsgTyp, (er, notified) => {
                                if (er) {
                                    res.json({
                                        success: false,
                                        msg: 'Error occured'
                                    })
                                }
                                if (notified) {
                                    res.json({
                                        success: true,
                                        msg: 'Post Liked'
                                    })
                                }
                            })
                        }
                        else {
                            res.json({
                                success: false,
                                msg: 'Failed to like post'
                            })
                        }
                    })
                } else {
                    Post.unlike(postId, username, (err3, isDone) => {
                        if (err3) {
                            res.json({
                                success: false,
                                msg: 'Error Occured'
                            })
                        }
                        if (isDone) {
                            res.json({
                                success: true,
                                msg: 'undo like'
                            })
                        } else {
                            res.json({
                                success: false,
                                msg: 'Failed to undo like'
                            })
                        }
                    })
                }
            }
        } else {
            res.json({
                success: false,
                msg: 'Post not found'
            })
        }
    })
})

router.get('/dislike/:postid', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const postId = req.params.postid;
    var isAlreadyDisliked = false;
    var hasLikedBefore = false;
    Post.getPostById(postId, (err2, thisPost) => {
        if (err2) {
            res.json({
                success: false,
                msg: 'Error occured'
            })
        }
        if (thisPost) {
            var getLikes = thisPost.likedBy;
            for (let lik of getLikes) {
                if (lik == username) {
                    hasLikedBefore = true;
                }
            }
            if (hasLikedBefore) {
                Post.likeToDislike(postId, username, (error, changed) => {
                    if (error) {
                        res.json({
                            success: false,
                            msg: 'Error Occured'
                        })
                    }
                    if (changed) {
                        const notifyMsg = username + ' disliked your post';
                        const notifyMsgTyp = 'dislike';
                        Notification.addNew(thisPost.user, notifyMsg, notifyMsgTyp, (er, notified) => {
                            if (er) {
                                res.json({
                                    success: false,
                                    msg: 'Error occured'
                                })
                            }
                            if (notified) {
                                res.json({
                                    success: true,
                                    msg: 'Post Disliked'
                                })
                            }
                        })
                    }
                    else {
                        res.json({
                            success: false,
                            msg: 'Failed to dislike'
                        })
                    }
                })
            }
            else {
                var dislikers = thisPost.dislikedBy;
                for (let disliker of dislikers) {
                    if (disliker == username) {
                        isAlreadyDisliked = true
                    }
                }
                if (!isAlreadyDisliked) {
                    Post.dislikePost(postId, username, (err, done) => {
                        if (err) {
                            res.json({
                                success: true,
                                msg: 'Error Occured'
                            })
                        }
                        if (done) {
                            const notifyMsg = username + ' disliked your post';
                            const notifyMsgTyp = 'dislike';
                            Notification.addNew(thisPost.user, notifyMsg, notifyMsgTyp, (er, notified) => {
                                if (er) {
                                    res.json({
                                        success: false,
                                        msg: 'Error occured'
                                    })
                                }
                                if (notified) {
                                    res.json({
                                        success: true,
                                        msg: 'Post Disliked'
                                    })
                                }
                            })
                        }
                        else {
                            res.json({
                                success: false,
                                msg: 'Failed to dislike post'
                            })
                        }
                    })
                } else {
                    Post.undislike(postId, username, (err3, isDone) => {
                        if (err3) {
                            res.json({
                                success: false,
                                msg: 'Error Occured'
                            })
                        }
                        if (isDone) {
                            res.json({
                                success: true,
                                msg: 'undo dislike'
                            })
                        } else {
                            res.json({
                                success: false,
                                msg: 'Failed to undo dislike'
                            })
                        }
                    })
                }
            }
        } else {
            res.json({
                success: false,
                msg: 'Post not found'
            })
        }
    })
})

router.post('/comment/:postid', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const postId = req.params.postid;
    const now = new Date();
    const timestamp = date.format(now, 'hh:mm A MMM DD YYYY');
    const newComment = {
        user: username,
        comment: req.body.comment,
        timestamp: timestamp
    }
    Post.getPostById(postId, (er, thisPost) => {
        if (er) {
            res.json({
                success: false,
                msg: 'Error occured'
            })
        }
        if (thisPost) {
            Post.addComment(postId, newComment, (err, done) => {
                if (err) {
                    res.json({
                        success: true,
                        msg: 'Error Occured'
                    })
                }
                if (done) {
                    const notifyMsg = username + ' commented on your post';
                    const notifyMsgTyp = 'comment';
                    Notification.addNew(thisPost.user, notifyMsg, notifyMsgTyp, (er2, notified) => {
                        if (er2) {
                            res.json({
                                success: false,
                                msg: 'Error occured'
                            })
                        }
                        if (notified) {
                            res.json({
                                success: true,
                                msg: 'Comment added'
                            })
                        }
                    })
                }
                else {
                    res.json({
                        success: false,
                        msg: 'Failed to add comment'
                    })
                }
            })
        } else {
            res.json({
                success: false,
                msg: 'Post not found'
            })
        }
    })
})

router.get('/deletecomment/:postid/:commentid', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const postId = req.params.postid;
    const commentId = req.params.commentid;
    var ownComment = false;
    var ownPost = false;
    Post.getPostById(postId, (err, post) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error Occured'
            })
        }
        if (post) {
            if (post.user == username) {
                ownPost = true;
            }
            if (ownPost) {
                Post.deleteComment(postId, commentId, (err2, delSuc) => {
                    if (err2) {
                        res.json({
                            success: false,
                            msg: 'Error Occured'
                        })
                    }
                    if (delSuc) {
                        res.json({
                            success: true,
                            msg: 'Comment deleted'
                        })
                    }
                    else {
                        res.json({
                            success: false,
                            msg: 'Failed to delete comment'
                        })
                    }
                })
            } else {
                const commentList = post.comments;
                for (let coms of commentList) {
                    if (coms._id == commentId && coms.user == username) {
                        ownComment = true
                    }
                }
                if (ownComment) {
                    Post.deleteComment(postId, commentId, (err3, del) => {
                        if (err3) {
                            res.json({
                                success: false,
                                msg: 'Error occured'
                            })
                        }
                        if (del) {
                            res.json({
                                success: true,
                                msg: 'Comment deleted'
                            })
                        } else {
                            res.json({
                                success: false,
                                msg: 'failed to delete comment'
                            })
                        }
                    })
                } else {
                    res.json({
                        success: false,
                        msg: 'Action not permitted'
                    })
                }
            }
        } else {
            res.json({
                success: false,
                msg: 'Post not found'
            })
        }
    })
})

router.get('/deletepost/:postid', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const postId = req.params.postid;
    const username = req.user.username;
    Post.getPostById(postId, (err, post) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error occured'
            })
        }
        if (post) {
            if (post.user == username) {
                Post.deletePost(postId, (err2, deleted) => {
                    if (err2) {
                        res.json({
                            success: false,
                            msg: 'Error occured'
                        })
                    }
                    if (deleted) {
                        res.json({
                            success: true,
                            msg: 'post deleted'
                        })
                    }
                    else {
                        res.json({
                            success: false,
                            msg: 'failed to delete post'
                        })
                    }
                })
            } else {
                res.json({
                    success: false,
                    msg: 'Action not permitted'
                })
            }

        } else {
            res.json({
                success: false,
                msg: 'Post not found'
            })
        }
    })
})

module.exports = router;