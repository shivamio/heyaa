const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

mongoose.connect(config.database);

mongoose.connection.on('connected',()=>{
    console.log('Connected to database '+config.database);
});

mongoose.connection.on('error', (err)=>{
    console.log('Database Error '+err)
});


const app = express();

const users = require('./routes/users');
const posts = require('./routes/posts');
const requests = require('./routes/requests');
const chats = require('./routes/chats');
const profiles = require('./routes/profiles');

const port = 3000;

app.use(cors());

app.use(express.static(path.join(__dirname,'public')));

app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);
app.use('/posts', posts);
app.use('/requests',requests);
app.use('/chats',chats);
app.use('/profiles',profiles);

app.get('/', (req,res)=>{
    res.send('Inivalid endpoint');
});

app.listen(port, ()=>{
    console.log('server started on '+port);
});
