const multer = require('multer');
const random = require('randomstring');
const path = require('path');

const storage = multer.diskStorage({
    destination:'./public/uploads/vid/posts',
    filename: function(req,file,cb){
        var randomName = random.generate({length:20});
        cb(null,Date.now()+'-'+randomName+path.extname(file.originalname))
    }
})

const uploadVidPost = multer({
    storage:storage
});

module.exports = uploadVidPost;