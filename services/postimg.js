const multer = require('multer');
const random = require('randomstring');
const path = require('path');

const storage = multer.diskStorage({
    destination:'./public/uploads/img/posts',
    filename: function(req,file,cb){
        var randomName = random.generate({length:20});
        cb(null,Date.now()+'-'+randomName+path.extname(file.originalname))
    }
})

const uploadImgPost = multer({
    storage:storage
});

module.exports = uploadImgPost;