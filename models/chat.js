const mongoose = require('mongoose');

const ChatSchema = mongoose.Schema({
    userA:{
        type:String,
        required:true
    },
    userB:{
        type:String,
        required:true
    },
    messages:[
        {
            to:String,
            from:String,
            timestamp:String,
            message:String,
            img:String,
            vid:String
        }
    ]
})

const Chat = module.exports = mongoose.model('Chat',ChatSchema);

module.exports.createChat = function(userA,userB,callback){
    var newChat = new Chat({
        userA:userA,
        userB:userB
    })
    newChat.save(callback);
}

module.exports.newMsg = function(userA,userB,msg, callback){
    Chat.findOneAndUpdate({$or:[{userA:userA,userB:userB},{userA:userB,userB:userA}]},{$push:{messages:msg}},callback)
}

module.exports.getAll = function(user, callback){
    Chat.find({$or:[{userA:user},{userB:user}]}, callback);
}

module.exports.between = function(userA, userB, callback){
    Chat.findOne({$or:[{userA:userA, userB:userB},{userB:userA, userA:userB}]}, callback);
}

