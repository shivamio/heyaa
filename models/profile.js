const mongoose = require('mongoose');

const ProfileSchema = mongoose.Schema({
    user:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    gender:{
        type:String,
        required:true
    },
    dob:{
        type:String,
        required:true
    },
    livingIn:{
        type:String,
        default:'Not Shared'
    },
    studiedAt:{
        type:String,
        default:'Not Shared'
    },
    workingAt:{
        type:String,
        default:'Not Shared'
    },
    profilePic:{
        type:String
    }
})

const Profile = module.exports = mongoose.model('Profile',ProfileSchema);

module.exports.create = function(newProfile, callback){
    newProfile.save(callback);
}

module.exports.getByUsername = function(user, callback){
    Profile.findOne({user:user}, callback);
}

module.exports.updateByusername = function(user, update, callback){
    Profile.findOneAndUpdate({user:user},update,callback);
}

module.exports.addDpToProfile = function(user, dpUrl, callback){
    Profile.findOneAndUpdate({user:user},{profilePic:dpUrl},callback);
}