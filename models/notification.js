const mongoose = require('mongoose');

const NotificationSchema = mongoose.Schema({
    user:{
        type:String,
        required:true
    },
    viewed:{
        type:Boolean,
        default:false
    },
    notificationMsg:{
        type:String,
        required:true
    },
    notificationType:{
        type:String
    },
    url:{
        type:String
    }
});

const Notification = module.exports = mongoose.model('Notification',NotificationSchema);

module.exports.addNew = function(user,msg,typ, callback){
    var notification = new Notification({
        user:user,
        notificationMsg:msg,
        notificationType:typ
    })
    notification.save(callback);
}

module.exports.getAll = function(user, callback){
    Notification.find({user:user}, callback);
}


module.exports.checkNew = function(user, callback){
    Notification.findOne({user:user, viewed:false},callback)
}

module.exports.getUnviewed = function(user, callback){
    Notification.find({user:user,viewed:false}, callback);
}


module.exports.allViewed = function(user, callback){
    Notification.update({user:user, viewed:false},{viewed:true},{multi:true},callback);
}


