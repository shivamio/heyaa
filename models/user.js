const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// User Schema
const UserSchema = mongoose.Schema({
    fname:{
        type:String,
        require: true
    },
    lname:{
        type:String,
        require: true
    },
    email:{
        type:String,
        require: true
    },
    username:{
        type:String,
        require:true
    },
    password:{
        type: String,
        require: true
    },
    connections:[
        {
            type:String
        }
    ]
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id, callback){
    User.findById(id, callback);
}

module.exports.getUserByUsername = function(username, callback){
    const query = {username, username}
    User.findOne(query, callback);
}
module.exports.addUser = function(newUser, callback){
    bcrypt.genSalt(10,(err, salt)=>{
        bcrypt.hash(newUser.password, salt, (err, hash)=>{
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        })
    })
}
module.exports.comparePassword = function(candidatePassword, hash, callback){
    bcrypt.compare(candidatePassword, hash, (err, isMatch)=>{
        if(err) throw err;
        callback(null, isMatch);
    });
}

module.exports.addConnection = function(userId, userToAdd, callback){
    User.findByIdAndUpdate(userId, {$push:{connections:userToAdd}}, callback)
}