const mongoose = require('mongoose');


const PostSchema = mongoose.Schema({
    user:{
        type:String,
        required:true
    },
    dateTime:{
        type:String,
        required:true
    },
    content:{
        type:String
    },
    img:{
       type:String 
    },
    vid:{
        type:String
    },
    postType:{
        type:String,
        default:'text'
    },
    likes:{
        type:Number,
        default:0
    },
    likedBy:[
        {
            type:String
        }
    ],
    dislikes:{
        type:Number,
        default:0
    },
    dislikedBy:[
        {
            type:String
        }
    ],
    comments:[
        {
            user:String,
            comment:String,
            timestamp:String
        }
    ]
})

const Post = module.exports = mongoose.model('Post', PostSchema);

module.exports.getByUsername = function(user, callback){
    const query = { user:user }
    Post.find(query, callback);
}

module.exports.addNew = function(newPost, callback){
    newPost.save(callback);
}

module.exports.getAll = function(userlist,callback){
    Post.find({user:{$in:userlist}}).sort({'_id':'desc'}).exec(callback);
}

module.exports.likePost = function(postId, user, callback){
    Post.findByIdAndUpdate(postId, {$push:{likedBy:user},$inc:{likes:1}}, callback);
}

module.exports.dislikePost = function(postId,user, callback){
    Post.findByIdAndUpdate(postId, {$push:{dislikedBy:user},$inc:{likes:-1}}, callback);
}

module.exports.addComment = function(postId,commentObj, callback){
    Post.findByIdAndUpdate(postId, {$push:{comments:commentObj}},callback);
}

module.exports.getPostById = function(id, callback){
    Post.findById(id,callback);
}

module.exports.likeToDislike = function(postId, user, callback){
    Post.findByIdAndUpdate(postId, {$push:{dislikedBy:user},$pull:{likedBy:user},$inc:{dislikes:1,likes:-1}},callback);
}

module.exports.dislikeToLike = function(postId, user, callback){
    Post.findByIdAndUpdate(postId, {$pull:{dislikedBy:user},$push:{likedBy:user},$inc:{dislikes:-1,likes:1}},callback);
}

module.exports.unlike = function(postId, user, callback){
    Post.findByIdAndUpdate(postId, {$pull:{likedBy:user},$inc:{likes:-1}},callback)
}

module.exports.undislike = function(postId, user, callback){
    Post.findByIdAndUpdate(postId, {$pull:{dislikedBy:user},$inc:{dislikes:-1}},callback)
}

module.exports.deleteComment = function(postId,commentId, callback){
    Post.findByIdAndUpdate(postId, {$pull:{comments:{_id:commentId}}},callback);
}

module.exports.deletePost = function(postId,callback){
    Post.findByIdAndRemove(postId, callback);
}