const mongoose = require('mongoose');
const date = require('date-and-time');

const RequestSchema = mongoose.Schema({
    to:{
        type:String,
        required:true
    },
    from:{
        type:String,
        required:true
    },
    dateTime:{
        type:String,
        required:true
    },
    approved:{
        type:Boolean,
        default:false
    }
})

const Request = module.exports = mongoose.model('Request',RequestSchema);

module.exports.new = function(toUser,fromUser, callback){
    var tempNow = new Date();
    var now = date.format(tempNow,'hh:mm A MMM DD YYYY');
    var newReq = new Request({
        to:toUser,
        from:fromUser,
        dateTime:now
    })
    newReq.save(callback);
}

module.exports.received = function(user, callback){
    Request.find({to:user,approved:false},callback);
}

module.exports.from = function(fromUser,toUser,callback){
    Request.findOne({from:fromUser,to:toUser, approved:false},callback);
}

module.exports.sent = function(user, callback){
    Request.find({from:user,approved:false},callback);
}

module.exports.approve = function(toUser,fromUser,callback){
    Request.findOneAndUpdate({to:toUser,from:fromUser},{approved:true},callback);
}

module.exports.deny = function(toUser,fromUser, callback){
    Request.findOneAndRemove({to:toUser, from:fromUser}, callback);
}